from app import app, db, models
from app.models import Pubdb
from flask import render_template, request, redirect, session
  
app.secret_key= 'pubbible'

#Creating the homepage that contains all created coursework assignments
@app.route('/', methods = ['POST','GET'])
def index():
        if  request.method == 'POST':
            user = request.form['user']
            session['user']= user
            return redirect('/form' )
        else:
            return render_template('index.html')

#using a form for user input, grabbing each form request and assigning it to the database
#before commit the changes and making them permanent
@app.route('/form', methods = ['POST','GET'])
def form():



    tasks= Pubdb.query.order_by(Pubdb.id)
    if  request.method == 'POST':
        pub_title = request.form['pub_name']
        vibes_score = request.form['vibes_rating']
        review = request.form['review_content']         

     

        pub_db = Pubdb( pub_name = pub_title, vibes_rating = vibes_score, review_content = review)


        try:
            db.session.add(pub_db)

            db.session.commit()
            return redirect('/form')
        except:
            return "an error has occur updating the database"
    else:
        return render_template('form.html', tasks = tasks)


@app.route('/arcadia', methods = ['POST','GET'])
def arcadia():
    if 'user' in session:
        user =  session['user']
    else:
        return redirect('/')
    tasks= Pubdb.query.order_by(Pubdb.id)
    if  request.method == 'POST':
        pub_title = 'Arcadia'
        vibes_score = request.form['vibes_rating']
        drinks_score = request.form['drinks_rating']
        price_score = request.form['price_rating']
        review = request.form['review_content']         

     

        pub_db = Pubdb(  pub_name = pub_title, vibes_rating = vibes_score, drinks_rating = drinks_score, price_rating = price_score, review_content = review, user_name= user)
        

        try:
            db.session.add(pub_db)

            db.session.commit()
            return redirect('/arcadia')
        except:
            
            return "an error has occur updating the database"
    else:
        pub_rev = Pubdb.query.filter_by(pub_name = 'Arcadia').all()
        return render_template('arcadia.html', tasks = pub_rev)


@app.route('/assembly', methods = ['POST','GET'])
def assembly():
    if 'user' in session:
        user =  session['user']
    else:
        return redirect('/')
    tasks= Pubdb.query.order_by(Pubdb.id)
    if  request.method == 'POST':
        pub_title = 'Assembly'
        vibes_score = request.form['vibes_rating']
        drinks_score = request.form['drinks_rating']
        price_score = request.form['price_rating']
        review = request.form['review_content']         

     

        pub_db = Pubdb(  pub_name = pub_title, vibes_rating = vibes_score, drinks_rating = drinks_score, price_rating = price_score, review_content = review, user_name= user)

        try:
            db.session.add(pub_db)

            db.session.commit()
            return redirect('/assembly')
        except:
            return "an error has occur updating the database"
    else:
        pub_rev = Pubdb.query.filter_by(pub_name = 'Assembly').all()
        return render_template('assembly.html', tasks = pub_rev)


@app.route('/brewdog', methods = ['POST','GET'])
def brewdog():
    if 'user' in session:
        user =  session['user']
    else:
        return redirect('/')
    tasks= Pubdb.query.order_by(Pubdb.id)
    if  request.method == 'POST':
        pub_title = 'Brewdog'
        vibes_score = request.form['vibes_rating']
        drinks_score = request.form['drinks_rating']
        price_score = request.form['price_rating']
        review = request.form['review_content']         

     

        pub_db = Pubdb(  pub_name = pub_title, vibes_rating = vibes_score, drinks_rating = drinks_score, price_rating = price_score, review_content = review, user_name= user)


        try:
            db.session.add(pub_db)

            db.session.commit()
            return redirect('/brewdog')
        except:
            return "an error has occur updating the database"
    else:
        pub_rev = Pubdb.query.filter_by(pub_name = 'Brewdog').all()
        return render_template('brewdog.html', tasks = pub_rev)




@app.route('/headofsteam', methods = ['POST','GET'])
def headofsteam():
    if 'user' in session:
        user =  session['user']
    else:
        return redirect('/')
    tasks= Pubdb.query.order_by(Pubdb.id)
    if  request.method == 'POST':
        pub_title = 'HoS'
        vibes_score = request.form['vibes_rating']
        drinks_score = request.form['drinks_rating']
        price_score = request.form['price_rating']
        review = request.form['review_content']         

     

        pub_db = Pubdb(  pub_name = pub_title, vibes_rating = vibes_score, drinks_rating = drinks_score, price_rating = price_score, review_content = review, user_name= user)

        try:
            db.session.add(pub_db)

            db.session.commit()
            return redirect('/headofsteam')
        except:
            return "an error has occur updating the database"
    else:
        pub_rev = Pubdb.query.filter_by(pub_name = 'HoS').all()
        return render_template('headofsteam.html', tasks = pub_rev)



#using queries to only access completed coursework
@app.route('/completed')
def complete():
    
    #finished = Pubdb.query.filter_by(cwk_completed = 'Finished').all()
    return render_template('completed.html', finished= finished)

#using queries to only access incomplete coursework
@app.route('/incomplete')
def incomplete():
    #unfinished = Pubdb.query.filter_by(cwk_completed = 'Unfinished').all()
    return render_template('incomplete.html' )#, unfinished= unfinished)



#adding a delete method to allow you to remove entries from the database
@app.route('/delete_coursework/<int:id>')
def delete(id):
    coursework_delete= Pubdb.query.get(id)
    
    try:
        db.session.delete(coursework_delete)
        db.session.commit()
        return redirect('/')
    except:
        return "Error unable to delete coursework submission"
   



#allowing you to update every part of the submission including completion status
@app.route('/update/<int:id>', methods = ['POST','GET'])
def update(id):
    coursework_update = Pubdb.query.get(id)
    
    if request.method == 'POST':
        
        coursework_update.pub_name = request.form['pub_name']
        coursework_update.vibes_rating = request.form['vibes_rating']
        coursework_update.review_content = request.form['review_content']         
        coursework_update.approval = request.form['approval']   
       
        try:   
            db.session.commit()
            return redirect('/')
        except:
            return "an error has occur updating the database"
    else:
        return render_template('update.html',coursework_update = coursework_update)  