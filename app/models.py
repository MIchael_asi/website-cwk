from app import db
from datetime import datetime

class Pubdb(db.Model):
    #creating sql database and initialising appropriate data types
    id = db.Column(db.Integer, primary_key=True)
    user_name= db.Column(db.String(200))
    review_content= db.Column(db.String(2000)) 
    drinks_rating = db.Column(db.String(2)) 
    vibes_rating = db.Column(db.String(2))
    price_rating = db.Column(db.String(2))
    
    pub_name= db.Column(db.String(10))
    approval = db.Column(db.String(10))


# a function to return a string when something is added to the database
    def __repr__(self) -> str:
        return '<Assignment %r>' % self.id
db.create_all()
